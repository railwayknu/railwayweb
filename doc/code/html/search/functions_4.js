var searchData=
[
  ['find_5ftown_5fin_5farray',['find_town_in_array',['../libalgol_8c.html#a7e051cf08f7947d9e00dc9382d4c5e8e',1,'libalgol.c']]],
  ['find_5ftowns',['find_towns',['../libalgol_8c.html#a9730fa1761d4acf7ef3a011098bc3fad',1,'find_towns(char **towns, int length, char *dep_town, char *dest_town, bool *ind, int *num_dep_town, int *num_dest_town):&#160;libalgol.c'],['../libalgol_8h.html#a9730fa1761d4acf7ef3a011098bc3fad',1,'find_towns(char **towns, int length, char *dep_town, char *dest_town, bool *ind, int *num_dep_town, int *num_dest_town):&#160;libalgol.c'],['../namespacealgol.html#aa51658f07791a6a82cd6f5974ef6c31a',1,'algol.find_towns()'],['../namespacebinding.html#aa8edacb138f22731defcb31f6c258ee5',1,'binding.find_towns()']]],
  ['find_5fuser_5fin_5fdatabase_5fusing_5fgoogle_5fid',['find_user_in_database_using_google_id',['../namespacedatabase.html#ac3bf97132acf2eef9367f86b3b8d6876',1,'database']]],
  ['find_5fuser_5fin_5fdatabase_5fusing_5fid',['find_user_in_database_using_id',['../namespacedatabase.html#afab327c20f8182f384c18269f39e5407',1,'database']]],
  ['find_5fuser_5fin_5fdatabase_5fusing_5flinkendin_5fid',['find_user_in_database_using_linkendin_id',['../namespacedatabase.html#a8c377b4ec234699640e37535f74cede0',1,'database']]],
  ['find_5fuser_5fin_5fdatabase_5fusing_5ftelegram_5fid',['find_user_in_database_using_telegram_id',['../namespacedatabase.html#a61de4c52f9f2de6337164f276768fc57',1,'database']]]
];
