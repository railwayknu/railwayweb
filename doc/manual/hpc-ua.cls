\message{HPC-UA 2011 Class}
\message{Modified CTU Poster 2011 Class}
\NeedsTeXFormat{LaTeX2e}%[1996/12/01]
\ProvidesClass{hpc-ua}
\RequirePackage{graphicx}

%\RequirePackage{fancyhdr}
\LoadClass[a4paper,oneside,10pt]{article}
\RequirePackage[utf8]{inputenc}
\RequirePackage[ukrainian]{babel}

\RequirePackage{geometry}
\geometry{left=2.5cm}
\geometry{right=1.5cm}
\geometry{top=1.5cm}
\geometry{bottom=2.5cm}

\headsep=9mm 
\parindent=0.5cm
\parskip=2mm %

\RequirePackage{setspace}
% Paragraph tuning
\singlespace 


\widowpenalty 10000
\clubpenalty 10000
\displaywidowpenalty 10000

%\def\@seccntformat#1{\csname the#1\endcsname.\space}
\def\@seccntformat#1{\csname the#1\endcsname\space}

\newcommand{\headerfont}{\footnotesize\sffamily\selectfont}
\newcommand{\abstractfont}{\normalsize\itshape\selectfont}
\newcommand{\titlefont}{\fontseries{b}\fontsize{20pt}{20pt} \selectfont}
\newcommand{\titleautfont}{\large\selectfont}
\newcommand{\titleaddfont}{\normalsize\itshape\selectfont}
\newcommand{\emailfont}{\normalsize\tt\selectfont}
\newcommand{\sectionfont}{\fontsize{14pt}{1}\fontseries{b}\fontshape{n}\selectfont}
\newcommand{\subsectionfont}{\large\fontseries{b}\selectfont}
\newcommand{\captionfont}{\normalsize\selectfont}
\newcommand{\tablefont}{\normalsize\bfseries\sffamily\selectfont}
\newcommand{\reffont}{\small\selectfont}

\columnsep=6mm

\emergencystretch=4em


\newcommand*\affiliation[1]{\def\@affiliation{#1}}
\newcommand*\affiliationmark[1]{\textsuperscript{#1}}
\newcommand*\email[1]{\def\em@il{#1}}

\title{TITLE here}
\author{AUTHOR here}
\affiliation{AFFILIATION here}
\email{EMAIL here}

\newbox\temp@box

\renewcommand\maketitle{%
  \par
  \begingroup
    \gdef\@thanks{}%
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark
        {\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
            \normalfont\@thefnmark~##1}%
    %\twocolumn[\@maketitle]%
    \@maketitle
    \@thanks
  \endgroup
  \setcounter{footnote}{0}%
  \setcounter{section}{0}%
  %\thispagestyle{first}%
}
\renewcommand\@maketitle{%
  \newpage
  \null
  \begin{center}% 
  \vspace*{-1.5mm}
%  \let \footnote \thanks
  {\titlefont 
  \@title \par}
  \vspace{2em}
  {
   \titleautfont
    \ignorespaces
   \@author
   \par}%
\vspace{1em}
  {
   \titleaddfont
   \ignorespaces
   \@affiliation\par}%
   \vspace{1em}
  {\emailfont
  \em@il\par}%
  \vspace{2em}
  \unhbox\temp@box
    \end{center}%
  \par
  \vspace{-5mm}
  }


\renewenvironment{abstract}
  {\parindent=0pt%
   \abstractfont%\
  \setlength{\baselineskip}{2.8ex}
  \textup{\textbf{Анотація.}}
}
{}

\newenvironment{keywords}{\section*{Ключові слова} 
\noindent
\hspace{0.75cm}
\begin{minipage}[t]{164mm}}{\end{minipage}}

\renewcommand{\belowcaptionskip}{0em}


\long\def\@makecaption#1#2{
  \vskip\abovecaptionskip
  \begin{center}
   \parbox[t]{0.75\textwidth}{{\captionfont\bfseries #1.~}{\captionfont #2}}
  \end{center}
  \vskip\belowcaptionskip
  \vskip1mm
}  

\setcounter{tocdepth}{0}
\pagestyle{empty}
\renewcommand{\figurename}{{%\captionfont 
Fig.}}
\renewcommand{\tablename}{{%\captionfont 
Tab.}}

