-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: railway
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add query',7,'add_query'),(26,'Can change query',7,'change_query'),(27,'Can delete query',7,'delete_query'),(28,'Can view query',7,'view_query'),(29,'Can add towns',8,'add_towns'),(30,'Can change towns',8,'change_towns'),(31,'Can delete towns',8,'delete_towns'),(32,'Can view towns',8,'view_towns'),(33,'Can add users',9,'add_users'),(34,'Can change users',9,'change_users'),(35,'Can delete users',9,'delete_users'),(36,'Can view users',9,'view_users'),(37,'Can add association',10,'add_association'),(38,'Can change association',10,'change_association'),(39,'Can delete association',10,'delete_association'),(40,'Can view association',10,'view_association'),(41,'Can add code',11,'add_code'),(42,'Can change code',11,'change_code'),(43,'Can delete code',11,'delete_code'),(44,'Can view code',11,'view_code'),(45,'Can add nonce',12,'add_nonce'),(46,'Can change nonce',12,'change_nonce'),(47,'Can delete nonce',12,'delete_nonce'),(48,'Can view nonce',12,'view_nonce'),(49,'Can add user social auth',13,'add_usersocialauth'),(50,'Can change user social auth',13,'change_usersocialauth'),(51,'Can delete user social auth',13,'delete_usersocialauth'),(52,'Can view user social auth',13,'view_usersocialauth'),(53,'Can add partial',14,'add_partial'),(54,'Can change partial',14,'change_partial'),(55,'Can delete partial',14,'delete_partial'),(56,'Can view partial',14,'view_partial');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'!MFpxTJXjlOITn1IiIpVYTY9eXyzOT8hYXhzpoSVo','2020-05-26 15:29:06.739641',0,'ArtemTomilo','Artem','Tomilo','',0,1,'2020-04-21 18:33:58.205922');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'railwayroute','query'),(8,'railwayroute','towns'),(9,'railwayroute','users'),(6,'sessions','session'),(10,'social_django','association'),(11,'social_django','code'),(12,'social_django','nonce'),(14,'social_django','partial'),(13,'social_django','usersocialauth');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2020-04-21 18:32:43.843917'),(2,'auth','0001_initial','2020-04-21 18:32:44.273434'),(3,'admin','0001_initial','2020-04-21 18:32:45.673284'),(4,'admin','0002_logentry_remove_auto_add','2020-04-21 18:32:46.191271'),(5,'admin','0003_logentry_add_action_flag_choices','2020-04-21 18:32:46.207239'),(6,'contenttypes','0002_remove_content_type_name','2020-04-21 18:32:46.436866'),(7,'auth','0002_alter_permission_name_max_length','2020-04-21 18:32:46.467525'),(8,'auth','0003_alter_user_email_max_length','2020-04-21 18:32:46.504104'),(9,'auth','0004_alter_user_username_opts','2020-04-21 18:32:46.517727'),(10,'auth','0005_alter_user_last_login_null','2020-04-21 18:32:46.631471'),(11,'auth','0006_require_contenttypes_0002','2020-04-21 18:32:46.639111'),(12,'auth','0007_alter_validators_add_error_messages','2020-04-21 18:32:46.666826'),(13,'auth','0008_alter_user_username_max_length','2020-04-21 18:32:46.694845'),(14,'auth','0009_alter_user_last_name_max_length','2020-04-21 18:32:46.734704'),(15,'auth','0010_alter_group_name_max_length','2020-04-21 18:32:46.767794'),(16,'auth','0011_update_proxy_permissions','2020-04-21 18:32:46.787848'),(17,'railwayroute','0001_initial','2020-04-21 18:33:21.437018'),(18,'sessions','0001_initial','2020-04-21 18:33:21.507575'),(19,'default','0001_initial','2020-04-21 18:33:22.042670'),(20,'social_auth','0001_initial','2020-04-21 18:33:22.050088'),(21,'default','0002_add_related_name','2020-04-21 18:33:22.489217'),(22,'social_auth','0002_add_related_name','2020-04-21 18:33:22.497492'),(23,'default','0003_alter_email_max_length','2020-04-21 18:33:22.525971'),(24,'social_auth','0003_alter_email_max_length','2020-04-21 18:33:22.533561'),(25,'default','0004_auto_20160423_0400','2020-04-21 18:33:22.565547'),(26,'social_auth','0004_auto_20160423_0400','2020-04-21 18:33:22.569627'),(27,'social_auth','0005_auto_20160727_2333','2020-04-21 18:33:22.636989'),(28,'social_django','0006_partial','2020-04-21 18:33:22.713060'),(29,'social_django','0007_code_timestamp','2020-04-21 18:33:22.899115'),(30,'social_django','0008_partial_timestamp','2020-04-21 18:33:23.075284'),(31,'social_django','0003_alter_email_max_length','2020-04-21 18:33:23.135385'),(32,'social_django','0001_initial','2020-04-21 18:33:23.139492'),(33,'social_django','0005_auto_20160727_2333','2020-04-21 18:33:23.149073'),(34,'social_django','0002_add_related_name','2020-04-21 18:33:23.157630'),(35,'social_django','0004_auto_20160423_0400','2020-04-21 18:33:23.167167');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('7crpqo5mgnhpcyotid8k127lve0tujke','NDJhMzY3ZDJkZGJjMzMxZGI5YWUwZmJlZjE5YTFhMjVlYTg2M2E3ODp7ImxpbmtlZGluLW9hdXRoMl9zdGF0ZSI6Ijg3dVNQT05BRERGODFlYzRSdEhCU0wwZllRNFByMHpaIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5saW5rZWRpbi5MaW5rZWRpbk9BdXRoMiIsIl9hdXRoX3VzZXJfaGFzaCI6IjA2ODY3ZjAzYTA1ZjMwZTBiYTIxZmRjZDFlNjJiZGIzYjUyODE1MTUiLCJzb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmQiOiJsaW5rZWRpbi1vYXV0aDIifQ==','2020-05-08 11:25:03.461973'),('bpwdhuo7pu61483gogxue2cexq9jwkpk','MmZhYTgwN2NkNmM5MGE4N2FiNGQ4NTkzOWI3YTZkMzAyNjMzNDA4MDp7ImxpbmtlZGluLW9hdXRoMl9zdGF0ZSI6IlR0UVlDaTh6OXpDZElnTzVmMmJkOUdYWjJ1WHdKOFRoIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5saW5rZWRpbi5MaW5rZWRpbk9BdXRoMiIsIl9hdXRoX3VzZXJfaGFzaCI6IjA2ODY3ZjAzYTA1ZjMwZTBiYTIxZmRjZDFlNjJiZGIzYjUyODE1MTUiLCJzb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmQiOiJsaW5rZWRpbi1vYXV0aDIifQ==','2020-05-05 18:33:58.273918'),('ku3srhk9r0uk5zqqyjbtackqxeuz7p20','ZmIzNjJlODIyMmYzZmIyNDFiNmFhYzRmNDYwNTUyMTkzOTc0N2I1Yjp7ImxpbmtlZGluLW9hdXRoMl9zdGF0ZSI6ImIwZkRxRTFMMUM5ZEVhWG5Qc3JCYVJoeTVKa25zdUx6IiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5saW5rZWRpbi5MaW5rZWRpbk9BdXRoMiIsIl9hdXRoX3VzZXJfaGFzaCI6IjA2ODY3ZjAzYTA1ZjMwZTBiYTIxZmRjZDFlNjJiZGIzYjUyODE1MTUiLCJzb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmQiOiJsaW5rZWRpbi1vYXV0aDIifQ==','2020-06-09 15:29:06.749101'),('yiiit5gqrwpcpfrutcqh6zglbziwsz2i','ZDAxMWE5NWVmZjI5ZDY3NzI1NDM4NzgwODA0NjQ0M2M2OTM3NDA3Yzp7ImxpbmtlZGluLW9hdXRoMl9zdGF0ZSI6IlBLYjF2Z3ZnVGdiekl4RlJGbVpHeURHaW9RRjVNZFJaIn0=','2020-06-09 15:19:15.501483');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `example`
--

DROP TABLE IF EXISTS `example`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `example` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `example`
--

LOCK TABLES `example` WRITE;
/*!40000 ALTER TABLE `example` DISABLE KEYS */;
INSERT INTO `example` VALUES (1,1,'Kyiv');
/*!40000 ALTER TABLE `example` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `railwayroute_query`
--

DROP TABLE IF EXISTS `railwayroute_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `railwayroute_query` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number_time_query` int(11) NOT NULL,
  `number_money_query` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `railwayroute_query`
--

LOCK TABLES `railwayroute_query` WRITE;
/*!40000 ALTER TABLE `railwayroute_query` DISABLE KEYS */;
INSERT INTO `railwayroute_query` VALUES (1,0,0);
/*!40000 ALTER TABLE `railwayroute_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `railwayroute_towns`
--

DROP TABLE IF EXISTS `railwayroute_towns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `railwayroute_towns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=321 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `railwayroute_towns`
--

LOCK TABLES `railwayroute_towns` WRITE;
/*!40000 ALTER TABLE `railwayroute_towns` DISABLE KEYS */;
INSERT INTO `railwayroute_towns` VALUES (301,1,'Kyiv'),(302,2,'Cherkasy'),(303,3,'Zhmerynka'),(304,4,'Kharkiv'),(305,5,'Kozyatyn'),(306,6,'Lviv'),(307,7,'Ivano-Frankivsk'),(308,8,'Colomia'),(309,9,'Rakhiv'),(310,10,'Mukachevo'),(311,11,'Odesa'),(312,12,'Dnipro'),(313,13,'Smela'),(314,14,'Rivne'),(315,15,'Zhytomyr'),(316,16,'Kryvyi Rih'),(317,17,'Chernivtsi'),(318,18,'Vinnytsia'),(319,19,'Sumy'),(320,20,'Poltava');
/*!40000 ALTER TABLE `railwayroute_towns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `railwayroute_users`
--

DROP TABLE IF EXISTS `railwayroute_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `railwayroute_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_telegram` int(11) DEFAULT NULL,
  `id_linkedin` varchar(256) DEFAULT NULL,
  `id_google` varchar(1024) DEFAULT NULL,
  `login` varchar(256) DEFAULT NULL,
  `password` int(11) DEFAULT NULL,
  `number_attempt` int(11) DEFAULT NULL,
  `number_time_query` int(11) DEFAULT NULL,
  `number_money_query` int(11) DEFAULT NULL,
  `type_query` varchar(256) DEFAULT NULL,
  `number_routes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `railwayroute_users`
--

LOCK TABLES `railwayroute_users` WRITE;
/*!40000 ALTER TABLE `railwayroute_users` DISABLE KEYS */;
INSERT INTO `railwayroute_users` VALUES (1,NULL,'Cl91wiwlNs',NULL,'bridge',NULL,15,0,0,'Default',8),(2,397280318,'',NULL,'bridgeArchitect',NULL,1,0,0,'Default',7);
/*!40000 ALTER TABLE `railwayroute_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_association`
--

DROP TABLE IF EXISTS `social_auth_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_url` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `issued` int(11) NOT NULL,
  `lifetime` int(11) NOT NULL,
  `assoc_type` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_association_server_url_handle_078befa2_uniq` (`server_url`,`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_association`
--

LOCK TABLES `social_auth_association` WRITE;
/*!40000 ALTER TABLE `social_auth_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_code`
--

DROP TABLE IF EXISTS `social_auth_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `code` varchar(32) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_code_email_code_801b2d02_uniq` (`email`,`code`),
  KEY `social_auth_code_code_a2393167` (`code`),
  KEY `social_auth_code_timestamp_176b341f` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_code`
--

LOCK TABLES `social_auth_code` WRITE;
/*!40000 ALTER TABLE `social_auth_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_nonce`
--

DROP TABLE IF EXISTS `social_auth_nonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_nonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_url` varchar(255) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `salt` varchar(65) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_nonce_server_url_timestamp_salt_f6284463_uniq` (`server_url`,`timestamp`,`salt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_nonce`
--

LOCK TABLES `social_auth_nonce` WRITE;
/*!40000 ALTER TABLE `social_auth_nonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_nonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_partial`
--

DROP TABLE IF EXISTS `social_auth_partial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_partial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) NOT NULL,
  `next_step` smallint(5) unsigned NOT NULL,
  `backend` varchar(32) NOT NULL,
  `data` longtext NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `social_auth_partial_token_3017fea3` (`token`),
  KEY `social_auth_partial_timestamp_50f2119f` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_partial`
--

LOCK TABLES `social_auth_partial` WRITE;
/*!40000 ALTER TABLE `social_auth_partial` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_partial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_usersocialauth`
--

DROP TABLE IF EXISTS `social_auth_usersocialauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_usersocialauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(32) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `extra_data` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_usersocialauth_provider_uid_e6b5e668_uniq` (`provider`,`uid`),
  KEY `social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id` (`user_id`),
  CONSTRAINT `social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_usersocialauth`
--

LOCK TABLES `social_auth_usersocialauth` WRITE;
/*!40000 ALTER TABLE `social_auth_usersocialauth` DISABLE KEYS */;
INSERT INTO `social_auth_usersocialauth` VALUES (1,'linkedin-oauth2','Cl91wiwlNs','{\"auth_time\": 1590506946, \"id\": \"Cl91wiwlNs\", \"expires\": 5184000, \"first_name\": {\"localized\": {\"en_US\": \"Artem\"}, \"preferredLocale\": {\"country\": \"US\", \"language\": \"en\"}}, \"last_name\": {\"localized\": {\"en_US\": \"Tomilo\"}, \"preferredLocale\": {\"country\": \"US\", \"language\": \"en\"}}, \"name\": null, \"email_address\": null, \"picture_url\": null, \"profile_url\": null, \"access_token\": \"AQVJhm87nHznB4eAJcp6k49XCkWZx0Si2PJ2dtQLpeaMtpWKJ1zPYGN0W27WMdTrkIFfc4N5WEqLoM6gzhT-ATysjqL3SZbuIDSDg5BSHQ9AHCwxGIdcOBMSvtOq2dO4y-lgaYm9bKFmLz3QQd8fmXqyISj-WjL28LlJmEMN_IEH_X1R8Ocb8SVRgjTeIJwUdjWnYSACrdxUc4P2_n2c7otvGyhShYuZT_wSIFKLuoLL3OGvLD8aTmdp8N19isg9z9tUi05-oA-RypB-c-0nJb85IBShcd6WSOh5o8Z6w3htZPC_mmiBia675UdJHqPG6prouhOuOj40N2MURF52RD0S2WmccQ\", \"token_type\": null}',1);
/*!40000 ALTER TABLE `social_auth_usersocialauth` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-31  9:37:06
