from django.shortcuts import redirect
from railwayroute import views


# name of Lindendin service for authentication
linkendin_auth = "linkedin-oauth2"
google_auth = "google-oauth2"

# function to save user data
def save_userdata(backend, user, response, *args, **kwargs):

    # import global variables
    global linkendin_auth
    global google_auth

    # check authentication server
    if backend.name == linkendin_auth:
        # receive user data and save it in variables of views.py
        views.linkendin_login = response["firstName"]["localized"]["en_US"]
        views.linkendin_id = response["id"]
        views.linkendin_is_authenticated = True

    if backend.name == google_auth:
        # receive user data and save it in variables of views.py
        views.google_login = response["name"]
        views.google_id = response["email"]
        views.google_is_authenticated = True
