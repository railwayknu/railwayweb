from django.apps import AppConfig


## Class for configuration.
#
#  This class contians basic parameters for configuration.
class RailwayrouteConfig(AppConfig):
    ## Class variable to save name of app.
    name = 'railwayroute'
