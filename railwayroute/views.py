## @package views
# This module has main logic of web site to show html-page.

# import django library
from django.shortcuts import render, redirect
from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)

# import library from modules
from modules import database
from modules import file
from modules import convert
from modules import check
from modules import binding

# import library for railwayweb
from railwayweb import settings

## array for towns (object)
towns = []
## transport graph
transport_graph = None
## transport routes
transport_routes = None
## default routes for initial query
default_routes = []
## name of map page
name_map_page = "map"
## name of login page
name_login_page = "login"
## name of personal page
name_personal_page = "personal"
## variables of user
user_variables = None
## temporary user
temp_user = {}
## variable to save number of queries
query = {}
## indicator of the first launch
first_launch = True
## temporary routes
temp_routes = []

## Telegram bot name
telegram_bot_name = settings.TELEGRAM_BOT_NAME
## Telegram bot token
telegram_bot_token = settings.TELEGRAM_BOT_TOKEN
## Telegram redirect url
telegram_redirect_url = settings.TELEGRAM_LOGIN_REDIRECT_URL

## Linkedin indicator for authentication
linkendin_is_authenticated = False
## Linkedin login
linkendin_login = ""
## Linkedin id
linkendin_id = ""

## Google indicator for authentication
google_is_authenticated = False
## Google login
google_login = ""
## Google id
google_id = ""

## This function initializes web service
#
# without any parameters
def init_web_service():

    # import global variables
    global transport_graph
    global towns
    global user_variables

    # initialize variables for binding
    binding.init_library()
    # read configuration file
    user_variables = file.read_config_file()
    # get towns from database
    towns = database.get_towns()
    # read transport graph
    transport_graph = file.read_graph_files(user_variables, len(towns))
    # create auxiliary arrays for ant algorithm
    binding.create_transport_routes()
    binding.create_ant_pheromones(transport_graph)

## This function initializes default routes.
#
#  user_variables is structure to save user parameters
def init_default_routes(url_address, number_routes):

    # import global variables
    global default_routes

    # set default values
    i = 0
    default_routes = []
    while i < number_routes:
        default_routes.append({"time": "Unknown", "towns": "Unknown",
                               "time_dep": "Unknown", "cost": "Unknown", "id": "Unknown",
                               "map": url_address})
        i = i + 1

## This Function creates data about routes for web service
#
# dep_town is departure town
#
# num_dep_town is id of departure town
#
# num_dest_town is id of destination town
#
# algol_data is id (row) to rule behaviour of ant algorithm
#
# user_id is id of user (not mandatory parameter)
def create_route_data(dep_town, num_dep_town, num_dest_town, algol_data, number_routes_user, user_id = None):

    # import global variables
    global user_variables
    global query
    global temp_user
    global transport_routes
    global temp_routes

    # receive number of queries for web service
    if user_id == None:
        query = database.get_number_queries()
    else:
        query = database.get_number_queries_user(user_id)

    # receive data about user if it is authenticated page
    if user_id != None:
        temp_user = database.find_user_in_database_using_id(user_id)

    # find routes
    if user_id == None:
        transport_routes = binding.search_routes(transport_graph, num_dep_town, num_dest_town, algol_data, query)
    else:
        transport_routes = binding.search_routes(transport_graph, num_dep_town, num_dest_town, temp_user.type_query,
                                                 query)

    # handle routes for web service
    if user_id == None:
        temp_routes = convert.make_postprocessing_routes(transport_routes, dep_town, transport_graph, towns, number_routes_user,
                                                    num_dep_town, user_variables, name_map_page, algol_data, query)
    else:
        temp_routes = convert.make_postprocessing_routes(transport_routes, dep_town, transport_graph, towns, number_routes_user,
                                                    num_dep_town, user_variables, name_map_page, temp_user.type_query,
                                                    query)

    # return routes
    return temp_routes

## This function clears ant algorithm for correct work
#
# without any parameters
def clear_ant_algol():

    # clear ant pheromones and routes
    binding.clear_transport_routes()
    binding.clear_ant_pheromones(transport_graph)

## Function adds one to number of query (general)
#
# algol_ind is type of query for adding
def add_query(algol_ind):

    # import global variables
    global user_variables

    # add number of time or money query
    if algol_ind == "Cost":
        database.add_money_query()
    elif algol_ind == "Time":
        database.add_time_query()


## This function adds one to number of query (concrete user)
#
# algol_ind is type of query for adding
#
# user_id is id of user
def add_query_personal(algol_ind, user_id):

    # import global variables
    global user_variables

    # add number of time or money query
    if algol_ind == "Cost":
        database.add_money_query_user(user_id)
    elif algol_ind == "Time":
        database.add_time_query_user(user_id)

## This function handles main page to send .html document
#
# request is parameter of http-request
def main_page(request):

    # import global variables
    global first_launch
    global towns
    global transport_graph
    global transport_routes
    global default_routes
    global user_variables

    # initialize of web service
    if first_launch:
        init_web_service()
        first_launch = False

    # check type of request
    if request.method == "POST":

        # receive towns from request
        dep_town = request.POST.get("departure")
        dest_town = request.POST.get("destination")
        # receive indicators for algorithm
        algol_ind = request.POST.get("choiceUser")

        # find towns in array
        full_ind, num_dep_town, num_dest_town = binding.find_towns(towns, dep_town, dest_town)
        # if towns exist
        if full_ind:
            # add one to number of queries (general)
            add_query(algol_ind)
            # create data about routes
            routes = create_route_data(dep_town, num_dep_town, num_dest_town, algol_ind,
                                       user_variables.num_routes_anom)
            # make view for html-page
            data = {"routes": routes,
                    "login_page": user_variables.url_address + "/" + name_login_page,
                    "towns": towns}
            # clear ant algorithm
            clear_ant_algol()
            # return html-page
            if check.check_mobile_browser(request):
                return render(request, "m_main_page.html", context=data)
            else:
                return render(request, "main_page.html", context=data)

        # make view for html-page
        data = {"routes": default_routes, "error": "Introduced towns are incorrect!",
                "login_page": user_variables.url_address + "/" + name_login_page,
                "towns": towns}
        # return html-page
        if check.check_mobile_browser(request):
            return render(request, "m_main_page.html", context=data)
        else:
            return render(request, "main_page.html", context=data)

    # initialize default routes
    init_default_routes(user_variables.url_address, user_variables.num_routes_anom)
    # make view for html-page
    data = {"routes": default_routes,
            "login_page": user_variables.url_address + "/" + name_login_page,
            "towns": towns}
    # return html-page
    if check.check_mobile_browser(request):
        return render(request, "m_main_page.html", context=data)
    else:
        return render(request, "main_page.html", context=data)

## This function handles map page to send .html document
#
# request is parameter of http-request
#
# row_towns is ids of towns to show route
def map_page(request, row_towns):

    # import global variables
    global user_variables

    # check referer using name for correct work of site
    if not check.check_referrer_main(request, user_variables.url_address):
        data = {"main_page": user_variables.url_address}
        # return html-page
        if check.check_mobile_browser(request):
            return render(request, "m_zero_page.html", context=data)
        else:
            return render(request, "zero_page.html", context=data)

    # create data for map page
    list_row = row_towns.split("&")
    i = 0
    list = []
    while i < len(list_row):
        list.append(int(list_row[i]))
        i = i + 1

    # fill data for map page
    data = {"list": list, "main_page": request.META.get('HTTP_REFERER')}

    # return html-page
    if check.check_mobile_browser(request):
        return render(request, "m_map_page.html", context=data)
    else:
        return render(request, "map_page.html", context=data)

## This function handles login page to send .html document
#
# request is parameter of http-request
def login_page(request):

    # import global variables
    global user_variables
    global linkendin_is_authenticated
    global linkendin_id
    global linkendin_login
    global google_is_authenticated
    global google_id
    global google_login
    global temp_user

    # check execution of Telegram authentication
    if request.GET.get('id'):
        # receive information from Telegram server
        try:
            result = verify_telegram_authentication(
                 bot_token=telegram_bot_token, request_data=request.GET
            )
        except TelegramDataIsOutdatedError:
            # handle exception of failed authentication
            data = {"main_page": user_variables.url_address, "error": "Authentication is failed!"}
            if check.check_mobile_browser(request):
                return render(request, "m_zero_page.html", context=data)
            else:
                return render(request, "zero_page.html", context=data)
        except NotTelegramDataError:
            # handle exception of failed connection
            data = {"main_page": user_variables.url_address, "error": "Connection with Telegram service is absent!"}
            if check.check_mobile_browser(request):
                return render(request, "m_zero_page.html", context=data)
            else:
                return render(request, "zero_page.html", context=data)
        # check user in database
        if not database.check_user_using_telegram_id(result["id"]):
            # add new user in database
            database.insert_new_user_telegram(result["id"], result["username"], user_variables.num_routes_user)
            # find user in database
            temp_user = database.find_user_in_database_using_telegram_id(result["id"])
        else:
            # find user in database
            temp_user = database.find_user_in_database_using_telegram_id(result["id"])
            # add number of attempt for user
            database.add_number_attempt_user(temp_user.id)
        # redirect to authenticated page
        return redirect(user_variables.url_address + "/auth/user_id=" + str(temp_user.id))

    # check execution of Linkendin authentication
    if linkendin_is_authenticated:
        # check user in database
        if not database.check_user_using_linkendin_id(linkendin_id):
            # add new user in database
            database.insert_new_user_linkendin(linkendin_id, linkendin_login, user_variables.num_routes_user)
            # find user in database
            temp_user = database.find_user_in_database_using_linkendin_id(linkendin_id)
        else:
            # find user in database
            temp_user = database.find_user_in_database_using_linkendin_id(linkendin_id)
            # add number of attempt for user
            database.add_number_attempt_user(temp_user.id)
        # redirect to authenticated page
        return redirect(user_variables.url_address + "/auth/user_id=" + str(temp_user.id))
    
    # check execution of Google authentication
    if google_is_authenticated:
        # check user in database
        if not database.check_user_using_google_id(google_id):
            # add new user in database
            database.insert_new_user_google(google_id, google_login, user_variables.num_routes_user)
            # find user in database
            temp_user = database.find_user_in_database_using_google_id(google_id)
        else:
            # find user in database
            temp_user = database.find_user_in_database_using_google_id(google_id)
            # add number of attempt for user
            database.add_number_attempt_user(temp_user.id)
        # redirect to authenticated page
        return redirect(user_variables.url_address + "/auth/user_id=" + str(temp_user.id))

    # check referer using name for correct work of site
    if not check.check_referrer_main(request, user_variables.url_address):
        data = {"main_page": user_variables.url_address}
        if check.check_mobile_browser(request):
            return render(request, "m_zero_page.html", context=data)
        else:
            return render(request, "zero_page.html", context=data)

    # fill data for login page
    data = {"main_page": request.META.get('HTTP_REFERER'),
            "url_address": user_variables.url_address + "/login",
            "name_bot": user_variables.name_bot}
    # return html-page
    if check.check_mobile_browser(request):
        return render(request, "m_login_page.html", context=data)
    else:
        return render(request, "login_page.html", context=data)

## This function handles main page for authenticated user to send .html document
#
# request is parameter of http-request
def auth_page(request, user_id):

    # import global variables
    global first_launch
    global towns
    global transport_graph
    global transport_routes
    global default_routes
    global user_variables
    global linkendin_is_authenticated
    global linkendin_id
    global linkendin_login
    global google_is_authenticated
    global google_id
    global google_login

    # check referer using name and indicator (Google and Linkedin) for correct work of site
    if (not check.check_referrer_login_and_map(request, user_variables.url_address)) and \
       (not check.check_referrer_main(request, user_variables.url_address)) and \
       (not check.check_referrer_personal(request, user_variables.url_address)) and \
       (not google_is_authenticated) and \
       (not linkendin_is_authenticated):
        data = {"main_page": user_variables.url_address}
        if check.check_mobile_browser(request):
            return render(request, "m_zero_page.html", context=data)
        else:
            return render(request, "zero_page.html", context=data)

    # finish Linkedin authentication
    linkendin_is_authenticated = False
    linkendin_id = ""
    linkendin_login = ""

    # finish Google authentication
    google_is_authenticated = False
    google_id = ""
    google_login = ""

    # receive user data from database using id
    user = database.find_user_in_database_using_id(int(user_id))

    # check type of request
    if request.method == "POST":

        # receive towns from request
        dep_town = request.POST.get("departure")
        dest_town = request.POST.get("destination")
        # receive indicators for algorithm
        algol_ind = request.POST.get("choiceUser")

        # find towns in array
        full_ind, num_dep_town, num_dest_town = binding.find_towns(towns, dep_town, dest_town)
        # if towns exist
        if full_ind:
            # add one to number of queries (general)
            add_query(algol_ind)
            # add one to number of queries (concrete user)
            add_query_personal(algol_ind, user_id)
            # create data about routes
            routes = create_route_data(dep_town, num_dep_town, num_dest_town,
                                       algol_ind, user.number_routes, user_id)
            # make view for html-page
            data = {"routes": routes,
                    "main_page": user_variables.url_address,
                    "login_page": user_variables.url_address + "/" + name_login_page,
                    "personal_page": user_variables.url_address + "/" + name_personal_page + "/user_id=" + user_id,
                    "login": user.login,
                    "towns": towns}
            # clear ant algorithm
            clear_ant_algol()
            # return html-page
            if check.check_mobile_browser(request):
                return render(request, "m_auth_page.html", context=data)
            else:
                return render(request, "auth_page.html", context=data)

        # make view for html-page
        data = {"routes": default_routes, "error": "Introduced towns are incorrect!",
                "main_page": user_variables.url_address,
                "login_page": user_variables.url_address + "/" + name_login_page,
                "personal_page": user_variables.url_address + "/" + name_personal_page + "/user_id=" + user_id,
                "login": user.login,
                "towns": towns}
        # return html-page
        if check.check_mobile_browser(request):
            return render(request, "m_auth_page.html", context=data)
        else:
            return render(request, "auth_page.html", context=data)

    # initialize default routes
    init_default_routes(user_variables.url_address + "/auth/user_id=" + user_id, user.number_routes)
    # make view for html-page
    data = {"routes": default_routes,
            "main_page": user_variables.url_address,
            "login_page": user_variables.url_address + "/" + name_login_page,
            "personal_page": user_variables.url_address + "/" + name_personal_page + "/user_id=" + user_id,
            "login": user.login,
            "towns": towns}
    # return html-page
    if check.check_mobile_browser(request):
        return render(request, "m_auth_page.html", context=data)
    else:
        return render(request, "auth_page.html", context=data)

## This function handles personal page to send .html document
#
# request is parameter of http-request
#
# user_id is id of user
def personal_page(request, user_id):

    # import global variables
    user_variables

    # check referer using name for correct work of site
    if (not check.check_referrer_main_auth(request, user_variables.url_address)) and \
       (not check.check_referrer_personal(request, user_variables.url_address)):
        data = {"main_page": user_variables.url_address}
        if check.check_mobile_browser(request):
            return render(request, "m_zero_page.html", context=data)
        else:
            return render(request, "zero_page.html", context=data)

    # receive user data from database using id
    user = database.find_user_in_database_using_id(int(user_id))

    # initialization of necessary local variables
    error = ""
    new_number_routes_int = 0

    # check type of request
    if request.method == "POST":

        # receive new name and indicator to change name
        name_change_ind = request.POST.get("nameChange")
        new_name = request.POST.get("newName")
        # receive indicator and type of query for algorithm
        algol_change_ind = request.POST.get("typeQueryChange")
        algol_ind = request.POST.get("choiceUser")
        # receive indicator and new number of queries
        number_routes_ind = request.POST.get("numberRoutesChange")
        new_number_routes = request.POST.get("newNumberRoutes")

        # check indicator to change name
        if name_change_ind != None:

            # check whether new name is empty
            if new_name != "":
                # change name of user
                database.change_name_user(int(user_id), new_name)
                # receive new user data from database using id
                user = database.find_user_in_database_using_id(int(user_id))
            else:
                # show error if new name is empty
                error = "Name should not be empty!"

        #  check indicator to change type of query
        if algol_change_ind != None:

            # change type of query
            database.change_type_query_for_user(int(user_id), algol_ind)
            # receive new user data from database using id
            user = database.find_user_in_database_using_id(int(user_id))

        #  check indicator to change number of routes
        if number_routes_ind != None:

            # transform new number of routes to int
            try:
                new_number_routes_int = int(new_number_routes)
            except Exception:
                error = "Number of routes should be correct number!"

            # check whether error exists
            if error == "":
                # check number of routes
                if (new_number_routes_int <= 10) & (new_number_routes_int >= 1):
                    # change number of routes
                    database.change_number_routes(int(user_id), new_number_routes_int)
                    # receive new user data from database using id
                    user = database.find_user_in_database_using_id(int(user_id))
                else:
                    # handle error with incorrect number of routes
                    error = "Number of routes should be number from 1 to 10!"

    # make view for html-page
    print(user.number_routes)
    data = {"auth_page": user_variables.url_address + "/auth/user_id=" + user_id,
            "main_page": user_variables.url_address,
            "login": user.login,
            "number_attempt": user.number_attempt,
            "number_time_query": user.number_time_query,
            "number_money_query": user.number_money_query,
            "number_routes": user.number_routes,
            "error": error}
    # return html-page
    if check.check_mobile_browser(request):
        return render(request, "m_personal_page.html", context=data)
    else:
        return render(request, "personal_page.html", context=data)
