/* import standart library */
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
/* include header file */
#include "libalgol.h"

/* function to find towns in array from database */
void find_towns(char **towns, int length, char * dep_town, char * dest_town, bool *ind,
                int *num_dep_town, int *num_dest_town) {

	/* number of towns in array */
    int num_dep_town_loc = -1;
    int num_dest_town_loc = -1;

    /* indicator of finding of towns */
    bool dep_ind_loc = false;
    bool dest_ind_loc = false;
    bool full_ind_loc = false;

    /* find departure town */
    for (int i = 0; i < length; i++) {
        if (!strcmp(towns[i], dep_town)) {
            dep_ind_loc = true;
            num_dep_town_loc = i;
            break;
        }
    }

    /* find destination town */
    for (int i = 0; i < length; i++) {
        if (!strcmp(towns[i], dest_town)) {
            dest_ind_loc = true;
            num_dest_town_loc = i;
            break;
        }
    }

    /* calculate full indicator */
    full_ind_loc = dep_ind_loc & dest_ind_loc;

    /* return answer */
    *num_dep_town = num_dep_town_loc;
    *num_dest_town = num_dest_town_loc;
    *ind = full_ind_loc;

}

/* function to create transport routes */
void create_transport_routes() {

    /* fill and create transport routes */
    transport_routes = (TransportRoute *) malloc(sizeof(TransportRoute) * number_ants);
    for (int i = 0; i < number_ants; i++) {
        transport_routes[i].cost = 0.0;
        transport_routes[i].time = 0;
        transport_routes[i].towns = (int *) malloc(sizeof(int) * number_towns);
        transport_routes[i].towns[0] = -1;
        /* create array of towns */
        for (int i1 = 1; i1 < number_towns; i1++) {
            transport_routes[i].towns[i1] = 0;
        }
    }

}

/* function to clear transport routes */
void clear_transport_routes() {

    /* clear transport routes */
    for (int i = 0; i < number_ants; i++) {
        transport_routes[i].cost = 0.0;
        transport_routes[i].time = 0;
        transport_routes[i].towns[0] = -1;
        /* clear array of towns */
        for (int i1 = 1; i1 < number_towns; i1++) {
            transport_routes[i].towns[i1] = 0;
        }
    }

}

/* function to create ant pheromones */
void create_ant_pheromones(int * transport_graph, int size) {

    /* create ant pheromones using data from transport graph */
    ant_pheromones = (double **) malloc(sizeof(double *) * size);
    for (int i = 0; i < size; i++) {
        ant_pheromones[i] = (double *) malloc(sizeof(double) * size);
        for (int j = 0; j < size; j++) {
            /* fill ant pheromones */
            if (transport_graph[i * size + j] == -1) {
                ant_pheromones[i][j] = -1.0;
            } else {
                ant_pheromones[i][j] = 1.0;
            }
        }
    }

}

/* function to clear ant pheromones */
void clear_ant_pheromones(int * transport_graph, int size) {

    /* clear ant pheromones using data from transport graph */
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            /* fill ant pheromones */
            if (transport_graph[i * size + j] == -1) {
                ant_pheromones[i][j] = -1.0;
            } else {
                ant_pheromones[i][j] = 1.0;
            }
        }
    }

}

/* function to calculate energy */
double calculate_energy(int * transport_graph, int size, int num_town) {

    /* calculate energy */
    double energy = 0.0;
    for (int i = 0; i < size; i++) {
        if (transport_graph[num_town * size + i] != -1) {
            energy += pow((double) transport_graph[num_town * size + i], 0.4) * pow(ant_pheromones[num_town][i], 0.6);
        }
    }

    /* return energy */
    return energy;

}

/* function to dry ant pheromones */
void dry_ant_pheromones(int size) {

    /* dry pheromones */
    for (int i = 0; i < size; i++) {
        for (int i1 = 0; i1 < size; i1++) {
            /* check existence of edge */
            if ((fabs(ant_pheromones[i][i1] + 1.0) > small_number) && (ant_pheromones[i][i1] / divide_constant > 1.0)) {
                ant_pheromones[i][i1] = ant_pheromones[i][i1] / divide_constant;
            }
        }
    }

}

/* function to sort routes using time data */
void sort_routes_by_time() {

    /* iterators */
    int i, i1, i2;
    /* current route for swapping */
    TransportRoute curr_route;

    /* launch Shell sort with optimal intervals */
    i = num_intervals - 1;
    while (i >= 0) {
        i1 = intervals[i];
        while (i1 < number_ants) {
            i2 = i1 - intervals[i];
            while (i2 >= 0) {
                /* swap routes */
                if (transport_routes[i2].time > transport_routes[i2 + intervals[i]].time) {
                    curr_route = transport_routes[i2];
                    transport_routes[i2] = transport_routes[i2 + intervals[i]];
                    transport_routes[i2 + intervals[i]] = curr_route;
                }
                i2 = i2 - intervals[i];
            }
            i1++;
        }
        i--;
    }

}

/* function to sort routes using money data */
void sort_routes_by_money() {

    /* iterators */
    int i, i1, i2;
    /* current route for swapping */
    TransportRoute curr_route;

    /* launch Shell sort with optimal intervals */
    i = num_intervals - 1;
    while (i >= 0) {
        i1 = intervals[i];
        while (i1 < number_ants) {
            i2 = i1 - intervals[i];
            while (i2 >= 0) {
                /* swap routes */
                if (transport_routes[i2].cost > transport_routes[i2 + intervals[i]].cost) {
                    curr_route = transport_routes[i2];
                    transport_routes[i2] = transport_routes[i2 + intervals[i]];
                    transport_routes[i2 + intervals[i]] = curr_route;
                }
                i2 = i2 - intervals[i];
            }
            i1++;
        }
        i--;
    }

}

/* function to sort routes using indicator of algorithm and type of query */
void sort_using_algol_ind(char * algol_ind, int query_time, int query_money) {

    /* sort using time or money */
    if (!strcmp(algol_ind, "Cost")) {
        sort_routes_by_money();
    } else if (!strcmp(algol_ind, "Time")) {
        sort_routes_by_time();
    } else if (!strcmp(algol_ind, "Default")) {
        /* default mode defines type of sorting using number of queries */
        if (query_time > query_money) {
            sort_routes_by_time();
        } else {
            sort_routes_by_money();
        }
    }

}

/* function to find town in array ("emulation" of set) */
bool find_town_in_array(int * set_towns, int length, int currNew) {

    /* initial value of indicator */
    bool ind = false;
    /* launch search */
    for (int i = 0; i < length; i++) {
        if (set_towns[i] == currNew) {
            ind = true;
            break;
        }
    }
    /* return answer */
    return ind;

}

/* function to search routes using ant algorithm */
TransportRoute * search_routes(int * transport_graph, double * transport_graph_cost,
                               int size, int num_dep_town, int num_dest_town, char * algol_ind,
                               int query_time, int query_money,
                               int * num_routes, int * num_towns) {

    /* current, new current and end town for search */
    int curr, currNew, end;
    /* iterators */
    int i, i1, i2;
    /* length of set */
    int length;
    /* array for emulation of set */
    int * set_towns;
    /* "energy" for part of route */
    double energy_full, energy_new;
    /* probability for new part */
    double prob, prob_comp;

    /* initialize random seed */
    if (first_launch) {
        srand(time(NULL));
        first_launch = false;
    }

    /* "launch" all ants */
    i = 0;
    while (i < number_ants) {

        /* save current and end towns */
        curr = num_dep_town;
        end = num_dest_town;
        /* set of towns for routes */
        set_towns = (int *) malloc(number_towns * sizeof(int));
        /* fill set of towns using default number */
        for (int j = 0; j < number_towns; j++) {
            set_towns[j] = -1;
        }
        /* initialization of iterators */
        length = 0;
        i1 = 0;
        i2 = -1;

        /* one ant starts to "move" to goal */
        while ((i1 < number_moves) & (curr != end)) {
            /* choose new current town */
            currNew = (int) (rand() % size);
            if ((!find_town_in_array(set_towns, length, currNew)) &&
                (transport_graph[curr * size + currNew] != -1)) {
                /* calculate full energy */
                energy_full = calculate_energy(transport_graph, size, curr);
                /* calculate energy of new town */
                energy_new = pow((double) transport_graph[curr * size + currNew], 0.4) *
                             pow((double) ant_pheromones[curr][currNew], 0.6);
                /* calculate probability */
                prob = energy_new / energy_full;
                prob_comp = ((double) rand())/((double) RAND_MAX);
                if (prob_comp < prob) {
                    /* add time to total counter with consideration of change */
                    if (i2 == -1) {
                        transport_routes[i].time += transport_graph[curr * size + currNew];
                    } else {
                        transport_routes[i].time += time_change + transport_graph[curr * size + currNew];
                    }
                    /* add cost of new direction to total cost */
                    transport_routes[i].cost += transport_graph_cost[curr * size + currNew];
                    /* record new town */
                    i2 = i2 + 1;
                    curr = currNew;
                    set_towns[length] = curr;
                    length++;
                    transport_routes[i].towns[i2] = curr;
                    transport_routes[i].towns[i2 + 1] = -1;
                }
            }
            i1++;
        }

        /* set big time for route that is impossible */
        if (curr != end) {
            transport_routes[i].time = big_time;
        }

        /* update ant pheromones */
        dry_ant_pheromones(size);
        if ((i2 >= 0) & (transport_routes[i].time != big_time)) {
            ant_pheromones[num_dest_town][transport_routes[i].towns[0]] =
                ant_pheromones[num_dest_town][transport_routes[i].towns[0]] +
                add_constant / (transport_routes[i].time * transport_routes[i].time * transport_routes[i].time);
            i1 = 0;
            while (i1 < i2) {
                ant_pheromones[transport_routes[i].towns[i1]][transport_routes[i].towns[i1 + 1]] =
                    ant_pheromones[transport_routes[i].towns[i1]][transport_routes[i].towns[i1 + 1]] +
                    add_constant / (transport_routes[i].time * transport_routes[i].time * transport_routes[i].time);
                i1++;
            }
        }

        /* clear set */
        free(set_towns);
        /* go through new iteration */
        i++;

    }

    /* sort routes using algorithm indicators */
    sort_using_algol_ind(algol_ind, query_time, query_money);
    /* return answer */
    *num_routes = number_ants;
    *num_towns = number_towns;
    return transport_routes;

}