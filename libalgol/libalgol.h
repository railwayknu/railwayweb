/**
 * @file libalgol.h
 * @brief Module to implement ant algorithm using C programming language
 */

/** It is structure to save transport routes */
typedef struct {
    /** array of towns to describe route */
    int * towns;
    /** time of route */
    int time;
    /** cost of route */
    double cost;
} TransportRoute;

/** number of ants */
const int number_ants = 7000;
/** number of towns during search */
const int number_towns = 40;
/** number of moves for ants */
const int number_moves = 400;
/** divide constant for drying */
const double divide_constant = 1.01;
/** small number for comparing */
const double small_number = 0.00001;

/** big time for route that does not exist */
const int big_time = 1000000;
/** constant to add pheromones during search */
const double add_constant = 100000.0;
/** time delay to change train */
const int time_change = 120;

/** intervals for Shell sort */
int intervals[8] = {1, 4, 10, 23, 57, 132, 301, 701};
/** number of intervals for Shell sort */
int num_intervals = 8;

/** array of transport routes (answer) */
TransportRoute * transport_routes;
/** ant pheromones for work of ant algorithm */
double ** ant_pheromones;

/** indicator of the firth launch */
bool first_launch = true;

/**
 * This function finds towns in array from database
 * @param towns is array of towns from database
 * @param length is length of array "towns"
 * @param dep_town is town of departure that gives user
 * @param dest_town is town of destination that gives user
 * @param ind is indicator of search success
 * @param num_dep_town is id of departure town that was found using "towns"
 * @param num_dest_town is id of destination town that was found using "towns"
 */
extern void find_towns(char **towns, int length, char * dep_town, char * dest_town, bool *ind,
                       int *num_dep_town, int *num_dest_town);

/**
 * This function creates transport routes (without any parameters)
 */
extern void create_transport_routes();

/**
 * This function clears transport routes (without any parameters)
 */
extern void clear_transport_routes();

/**
 * This function creates array for ant pheromones
 * @param transport_graph is two-dimensional array to describe transport graph (time)
 * @param size is size of two-dimensional array
 */
extern void create_ant_pheromones(int * transport_graph, int size);

/**
 * This function clears array for ant pheromones
 * @param transport_graph is two-dimensional array to describe transport graph (time)
 * @param size is size of two-dimensional array
 */
extern void clear_ant_pheromones(int * transport_graph, int size);

/**
  * This function to search routes using ant algorithm
  * @param transport_graph is two-dimensional array to describe transport graph (time)
  * @param transport_graph_cost is two-dimensional array to describe transport graph (money)
  * @param size is size of two-dimensional array
  * @param num_dep_town is id of departure town
  * @param num_dest_town is id of destination town
  * @param algol_ind is indicator to choose type of sorting
  * @param query_time is number of time query
  * @param query_money is number of money query
  * @param num_routes is number of routes in "TransportRoute" (answer)
  * @param num_towns is number of towns in "TransportRoute.towns" (answer)
  */
extern TransportRoute * search_routes(int * transport_graph, double * transport_graph_cost,
                                      int size, int num_dep_town, int num_dest_town,
                                      char * algol_ind, int query_time, int query_money,
                                      int * num_routes, int * num_towns);

