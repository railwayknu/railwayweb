## @package convert
# This module has functions to convert different objects to row

## big time for routes that do not exist
big_time = 1000000

## Thsi function creates row from time (integer number)
#
# time is number of minutes to describe time of day
#
# return row of converting
def create_row_from_time(time):

    # convert to string
    if time % 60 < 10:
        answer = str(int(time / 60)) + ":0" + str(time % 60)
    else:
        answer = str(int(time / 60)) + ":" + str(time % 60)

    # return answer
    return answer

## This function creates row from towns (list of towns to describe route)
#
# transport_routes is list of found routes by ant algorithm
#
# dep_town is departure town
#
# towns is list of towns which is received from database
#
# index is number of route in transport_routes
#
# return row of converting
def create_row_from_towns(transport_routes, dep_town, towns, index):

    # create rows from ids of towns
    row_town = dep_town + " "
    i = 0
    while transport_routes[index].towns[i] != -1:
        tuple = towns[transport_routes[index].towns[i]]
        row_town += tuple["name"] + " "
        i = i + 1
    return row_town

## This function creates row from time of departure (integer number)
#
# transport_routes is list of found routes by ant algorithm
#
# transport graph is two-dimensional array to describe graph of Ukrainian railway
#
# index is number of route in transport_routes
#
# num_dep_town is id of departure town
#
# return row of converting
def create_row_from_time_dep(transport_routes, transport_graph, index, num_dep_town):

    # receive time of departure
    time_dep = transport_graph.time_dep[num_dep_town][transport_routes[index].towns[0]]
    # convert to string
    if time_dep % 60 < 10:
        answer = str(int(time_dep / 60)) + ":0" + str(time_dep % 60)
    else:
        answer = str(int(time_dep / 60)) + ":" + str(time_dep % 60)

    # return answer
    return answer

## This function creates row from costs of trip (real numbers)
#
# transport_routes is list of found routes by ant algorithm
#
# transport graph is two-dimensional array to describe graph of Ukrainian railway
#
# index is number of route in transport_routes
#
# num_dep_town is id of departure town
#
# return row of converting
def create_row_from_cost(transport_routes, transport_graph, index, num_dep_town):

    # go through all towns of route and find cost
    cost = transport_graph.cost[num_dep_town][transport_routes[index].towns[0]]
    i = 0
    while i < len(transport_routes[index].towns) - 2:
        cost += transport_graph.cost[transport_routes[index].towns[i]][transport_routes[index].towns[i + 1]]
        i = i + 1

    # return answer
    return str(round(cost, 2)) + " UAH"

## This function creates row from id ant types of trip (integer numbers)
#
# transport_routes is list of found routes by ant algorithm
#
# transport graph is two-dimensional array to describe graph of Ukrainian railway
#
# index is number of route in transport_routes
#
# num_dep_town is id of departure town
#
# return row of converting
def create_row_from_id_and_type(transport_routes, transport_graph, index, num_dep_town):

    # go through initial towns of route and fill row
    id_cur = transport_graph.id[num_dep_town][transport_routes[index].towns[0]]
    type_cur = transport_graph.type[num_dep_town][transport_routes[index].towns[0]]
    if type_cur == 1:
        answer = str(id_cur) + "П "
    else:
        answer = str(id_cur) + "К "

    # go through all towns of route and fill row
    i = 0
    while i < len(transport_routes[index].towns) - 2:
        id_cur = transport_graph.id[transport_routes[index].towns[i]][transport_routes[index].towns[i + 1]]
        type_cur = transport_graph.type[transport_routes[index].towns[i]][transport_routes[index].towns[i + 1]]
        if type_cur == 1:
            answer += str(id_cur) + "П "
        else:
            answer += str(id_cur) + "К "
        i = i + 1

    # return answer
    return answer

## This function creates row from id of towns (integer numbers)
#
# name_map_page is URL-address of map page (beginning of row of town's id)
#
# transport_routes is list of found routes by ant algorithm
#
# index is number of route in transport_routes
#
# num_dep_town is id of departure town
#
# return row of converting
def create_row_from_town_ids(name_map_page, transport_routes, index, num_dep_town):

    # go through all towns of route to find ids of town
    answer = name_map_page + "/" + str(num_dep_town) + "&"
    i = 0
    while i < len(transport_routes[index].towns) - 2:
        answer += str(transport_routes[index].towns[i]) + "&"
        i = i + 1
    answer += str(transport_routes[index].towns[len(transport_routes[index].towns) - 2]) + "/"

    # return answer
    return answer

## This function contains main login to transform internal structure data into rows for comfortable perception (time)
#
# transport_routes is list of found routes by ant algorithm
#
# dep_town is departure town
#
# transport graph is two-dimensional array to describe graph of Ukrainian railway
#
# towns is list of towns from database
#
# number_routes_user is number routes that will show for users
#
# num_dep_town is id of departure town
#
# user_variables is structure to save user's configuration
#
# name_map_page is URL-address of map page (beginning of row of town's id)
#
# return list of routes after postprocessing
def make_postprocessing_routes_time(transport_routes, dep_town, transport_graph, towns, number_routes_user, num_dep_town,
                                    user_variables, name_map_page):

    # routes for users
    routes = []

    # handle routes
    i = 0
    i1 = 0
    while (i1 < number_routes_user) & (i < len(transport_routes) - 1):
        # find new route
        if (transport_routes[i].time != transport_routes[i + 1].time):
            # add new route
            routes.append({"time": create_row_from_time(transport_routes[i].time),
                           "towns": create_row_from_towns(transport_routes, dep_town, towns, i),
                           "time_dep": create_row_from_time_dep(transport_routes, transport_graph,
                                                                        i, num_dep_town),
                           "cost": create_row_from_cost(transport_routes, transport_graph,
                                                                i, num_dep_town),
                           "id": create_row_from_id_and_type(transport_routes, transport_graph,
                                                                     i, num_dep_town),
                           "map": user_variables.url_address + "/" + create_row_from_town_ids(name_map_page,
                                                                     transport_routes, i, num_dep_town)})
            i1 = i1 + 1
        i = i + 1

    # return routes
    return routes

## This function contains main login to transform internal structure data into rows for comfortable perception (money)
#
# transport_routes is list of found routes by ant algorithm
#
# dep_town is departure town
#
# transport graph is two-dimensional array to describe graph of Ukrainian railway
#
# towns is list of towns from database
#
# number_routes_user is number routes that will show for users
#
# num_dep_town is id of departure town
#
# user_variables is structure to save user's configuration
#
# name_map_page is URL-address of map page (beginning of row of town's id)
#
# return list of routes after postprocessing
def make_postprocessing_routes_money(transport_routes, dep_town, transport_graph, towns, number_routes_user, num_dep_town,
                                    user_variables, name_map_page):
    # routes for users
    routes = []

    # handle routes
    i = 0
    i1 = 0
    while (i1 < number_routes_user) & (i < len(transport_routes) - 1):
        # find new route
        if (transport_routes[i].cost != transport_routes[i + 1].cost) & (transport_routes[i].time != big_time):
            # add new route
            routes.append({"time": create_row_from_time(transport_routes[i].time),
                           "towns": create_row_from_towns(transport_routes, dep_town, towns, i),
                           "time_dep": create_row_from_time_dep(transport_routes, transport_graph,
                                                                i, num_dep_town),
                           "cost": create_row_from_cost(transport_routes, transport_graph,
                                                        i, num_dep_town),
                           "id": create_row_from_id_and_type(transport_routes, transport_graph,
                                                             i, num_dep_town),
                           "map": user_variables.url_address + "/" + create_row_from_town_ids(name_map_page,
                                                                                              transport_routes, i,
                                                                                              num_dep_town)})
            i1 = i1 + 1
        i = i + 1

    # return routes
    return routes

## This function contains main login to transform internal structure data into rows for comfortable perception (general)
#
# transport_routes is list of found routes by ant algorithm
#
# dep_town is departure town
#
# transport graph is two-dimensional array to describe graph of Ukrainian railway
#
# towns is list of towns from database
#
# number_routes_user is number routes that will show for users
#
# num_dep_town is id of departure town
#
# user_variables is structure to save user's configuration
#
# name_map_page is URL-address of map page (beginning of row of town's id)
#
# return list of routes after postprocessing
def make_postprocessing_routes(transport_routes, dep_town, transport_graph, towns, number_routes_user, num_dep_town,
                               user_variables, name_map_page, algol_ind, query):

    # initial value
    routes = []

    # make postprocessing using time or money
    if algol_ind == "Cost":
        routes = make_postprocessing_routes_money(transport_routes, dep_town, transport_graph, towns,
                                                  number_routes_user, num_dep_town,
                                                  user_variables, name_map_page)
    elif algol_ind == "Time":
        routes = make_postprocessing_routes_time(transport_routes, dep_town, transport_graph, towns,
                                                 number_routes_user, num_dep_town,
                                                 user_variables, name_map_page)
    elif algol_ind == "Default":
        # default mode defines type of postprocessing using number of queries
        if query.number_time_query > query.number_money_query:
            routes = make_postprocessing_routes_time(transport_routes, dep_town, transport_graph, towns,
                                                     number_routes_user, num_dep_town,
                                                     user_variables, name_map_page)
        else:
            routes = make_postprocessing_routes_money(transport_routes, dep_town, transport_graph, towns,
                                                      number_routes_user, num_dep_town,
                                                      user_variables, name_map_page)

    # return answer
    return routes
