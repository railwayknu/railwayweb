## @package database
# This module has all functions to interact with database (MySQL)

from railwayroute.models import RailwayrouteTowns
from railwayroute.models import RailwayrouteQuery
from railwayroute.models import RailwayrouteUsers

## id of main record to receive data about queries
id_query = 1

## This function gives data about towns from MySQL database
#
# without any parameters
#
# return array of objects that contain information about towns
def get_towns():

    response = []
    for record in RailwayrouteTowns.objects.all():
        response.append({
            "number": record.number,
            "name": record.name})

    return response

## This function gives data about number of queries (different types) from database
#
# without any parameters
#
# return one object that contains information about queries
def get_number_queries():
    return RailwayrouteQuery.objects.all()[0]

## This function gives data about number of queries (different types) for one concrete user from database
#
# user_id is general id of user in database
#
# return one object that contains information about queries
def get_number_queries_user(user_id):
    return RailwayrouteUsers.objects.get(id=user_id)

## This function inserts new user in database using Telegram account
#
# user_id is Telegram id of user in database
#
# user_login is name of user in database
#
# number_routes is number of routes for showing
def insert_new_user_telegram(user_id, user_login, number_routes):

    RailwayrouteUsers.objects.create(
        id_telegram = user_id,
        login = user_login,
        number_attempt = 1,
        number_time_query = 0,
        number_money_query = 0,
        type_query = "Default",
        number_routes = number_routes)

## This function inserts new user in database using Linkedin account
#
# user_id is Linkedin id of user in database
#
# user_login is name of user in database
#
# number_routes is number of routes for showing
def insert_new_user_linkendin(user_id, user_login, number_routes):

    RailwayrouteUsers.objects.create(
        id_linkedin = user_id,
        login = user_login,
        number_attempt = 1,
        number_time_query = 0,
        number_money_query = 0,
        type_query = "Default",
        number_routes = number_routes)


## This function inserts new user in database using Google account
#
# user_id is Google id of user in database
#
# user_login is name of user in database
#
# number_routes is number of routes for showing
def insert_new_user_google(user_id, user_login, number_routes):

    RailwayrouteUsers.objects.create(
        id_google = user_id,
        login = user_login,
        number_attempt = 1,
        number_time_query = 0,
        number_money_query = 0,
        type_query = "Default",
        number_routes = number_routes)


## This function checks user in database using general id
#
# user_id is general id of user in database
#
# return boolean indicator of checking
def check_user_using_id(user_id):
    return RailwayrouteUsers.objects.filter(id=user_id).exists()


## This function checks user in database using Telegram id
#
# user_id is Telegram id of user in database
#
# return boolean indicator of checking
def check_user_using_telegram_id(user_id):
    return RailwayrouteUsers.objects.filter(id_telegram = user_id).exists()


## This function checks user in database using Telegram id
#
# user_id is Linkedin id of user in database
#
# return boolean indicator of checking
def check_user_using_linkendin_id(user_id):
    return RailwayrouteUsers.objects.filter(id_linkedin=user_id).exists()


## This function checks user in database using Telegram id
#
# user_id is Google id of user in database
#
# return boolean indicator of checking
def check_user_using_google_id(user_id):
    return RailwayrouteUsers.objects.filter(id_google=user_id).exists()


## This function finds user in database using general id
#
# user_id is general id of user in database
#
# return object of user
def find_user_in_database_using_id(user_id):
    return RailwayrouteUsers.objects.get(id=user_id)

## This function finds user in database using Telegram id
#
# user_id is Telagram id of user in database
#
# return object of user
def find_user_in_database_using_telegram_id(user_id):
    return RailwayrouteUsers.objects.get(id_telegram = user_id)


## This function finds user in database using Linkedin id
#
# user_id is Linkedin id of user in database
#
# return object of user
def find_user_in_database_using_linkendin_id(user_id):
    return RailwayrouteUsers.objects.get(id_linkedin = user_id)


## This function finds user in database using Google id
#
# user_id is Google id of user in database
#
# return object of user
def find_user_in_database_using_google_id(user_id):
    return RailwayrouteUsers.objects.get(id_google = user_id)


## This function changes name of concrete user
#
# user_id is general id of user in database
#
# new_name is new name of user
def change_name_user(user_id, new_name):
    user = RailwayrouteUsers.objects.get(id=user_id)
    user.login = new_name
    user.save(update_fields=["login"])

## This function changes default type of query for concrete user
#
# user_id is general id of user in database
#
# new_query is new type of query
def change_type_query_for_user(user_id, new_query):
    user = RailwayrouteUsers.objects.get(id = user_id)
    user.type_query = new_query
    user.save(update_fields=["type_query"])

## This function changes number of routes for concrete user
#
# user_id is general id of user in database
#
# new_number_routes is new number of routes
def change_number_routes(user_id, new_number_routes):
    user = RailwayrouteUsers.objects.get(id = user_id)
    user.number_routes = new_number_routes
    user.save(update_fields=["number_routes"])

## This function adds one to number of attempts for concrete user
#
# user_id is general id of user in database
def add_number_attempt_user(user_id):
    record = RailwayrouteUsers.objects.get(id=user_id)
    record.number_attempt = record.number_attempt + 1
    record.save()

## This function adds one to number of time queries for concrete user
#
# user_id is general id of user in database
def add_time_query_user(user_id):
    record = RailwayrouteUsers.objects.get(id = user_id)
    record.number_time_query = record.number_time_query + 1
    record.save()

## This function adds one to number of money queries for concrete user
#
# user_id is general id of user in database
def add_money_query_user(user_id):
    record = RailwayrouteUsers.objects.get(id = user_id)
    record.number_money_query = record.number_money_query + 1
    record.save()

## This function adds one to general number of time queries
#
# without any parameters
def add_time_query():
    record = RailwayrouteQuery.objects.get(id = id_query)
    record.number_time_query = record.number_time_query + 1
    record.save()

## This function adds one to general number of money queries
#
# without any parameters
def add_money_query():
    record = RailwayrouteQuery.objects.get(id = id_query)
    record.number_money_query = record.number_money_query + 1
    record.save()

