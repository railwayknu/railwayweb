## @package algol
# This module has all functions to find routes using ant algorithm

# import library to generate random pages
from random import randint
from random import random

## number of ants
number_ants = 500
## number of moves for one ant
number_moves = 1000
## time for changing of train (in minutes)
time_change = 120
## array for transport routes which are found by ant algorithm
transport_routes = []
## big time in system that means impossibility of route
big_time = 1000000
## two-dimensional array for ant pheromones
ant_pheromones = []
## small number for comparing
small_number = 0.00001
## constant of dividing (ant pheromones)
divide_constant = 1.01
## constant of adding (ant pheromones)
add_constant = 100000
## intervals for Shell sort
interval = [1, 4, 10, 23, 57, 132, 301, 701]

## class to describe transport route
class TransportRoute:

    ##__init__() class constructor
    def __init__(self):
        self.towns = []
        self.time = 0
        self.cost = 0.0

## This function finds town in array from database
#
# towns is array of towns from database
#
# dep_town is name of departure town
#
# dest_town is name of destination town
#
# return indicator of success and id of towns in array
def find_towns(towns, dep_town, dest_town):

    # number of towns in array
    num_dep_town = -1
    num_dest_town = -1

    # indicator of finding of towns
    dep_ind = False
    dest_ind = False

    # find departure town
    i = 0
    while i < len(towns):
        if towns[i]["name"] == dep_town:
            dep_ind = True
            num_dep_town = i
        i = i + 1

    # find destination town
    i = 0
    while i < len(towns):
        if towns[i]["name"] == dest_town:
            dest_ind = True
            num_dest_town = i
        i = i + 1

    # return answer
    full_ind = dep_ind & dest_ind
    return full_ind, num_dep_town, num_dest_town

## This function creates transport routes for ant algorithm
#
# without any parameters
def create_transport_routes():

    # add transport routes for ants
    i = 0
    while i < number_ants:
        transport_routes.append(TransportRoute())
        transport_routes[i].towns.append(-1)
        i = i + 1

## This function clears transport routes for ant algorithm
#
# without any parameters
def clear_transport_routes():

    # clear transport routes for further work
    i = 0
    while i < number_ants:
        transport_routes[i].towns = []
        transport_routes[i].towns.append(-1)
        transport_routes[i].time = 0
        transport_routes[i].cost = 0.0
        i = i + 1

## This function creates two-dimensional array for ant pheromones
#
# without any parameters
def create_ant_pheromones(transport_graph):

    # set initial values of ant pheromones
    i = 0
    while i < len(transport_graph.time_travel):
        temp_array = []
        i1 = 0
        while i1 < len(transport_graph.time_travel):
            # check existence of edge
            if transport_graph.time_travel[i][i1] == -1:
                temp_array.append(-1.0)
            else:
                temp_array.append(1.0)
            i1 = i1 + 1
        ant_pheromones.append(temp_array)
        i = i + 1

## This function clears two-dimensional array for ant pheromones
#
# without any parameters
def clear_ant_pheromones(transport_graph):

    # clear ant pheromones
    i = 0
    while i < len(transport_graph.time_travel):
        i1 = 0
        while i1 < len(transport_graph.time_travel):
            # check existence of edge
            if transport_graph.time_travel[i][i1] == -1:
                ant_pheromones[i][i1] = -1.0
            else:
                ant_pheromones[i][i1] = 1.0
            i1 = i1 + 1
        i = i + 1

## This function calculates "energy" (this parameter impacts on possibility of concrete route)
#
# transport_graph is transport graph to describe connection between towns
#
# ant_pheromones is array to save ant pheromones
#
# num_towm is id of concrete town
#
# return "energy" for concrete town
def calculate_energy(transport_graph, ant_pheromones, num_town):

    # initial values
    energy = 0.0
    i = 0
    # calculate "energy" for given town
    while i < len(transport_graph.time_travel):
        if transport_graph.time_travel[num_town][i] != -1:
            energy = energy + (transport_graph.time_travel[num_town][i] ** 0.4) * (ant_pheromones[num_town][i] ** 0.6)
        i = i + 1

    # return energy
    return energy

## This function dries ant pheromones to execute algorithm
#
# without any parameters
def dry_ant_pheromones():

    # import global variables
    global divide_constant

    # make drying of ant pheromones
    i = 0
    while i < len(ant_pheromones):
        i1 = 0
        while i1 < len(ant_pheromones):
            # check existence of edge and value of pheromones
            if (abs(ant_pheromones[i][i1] + 1.0) > small_number) & (ant_pheromones[i][i1] / divide_constant > 1.0):
                ant_pheromones[i][i1] = ant_pheromones[i][i1] / divide_constant
            i1 = i1 + 1
        i = i + 1

## This function sorts routes by time
#
# without any parameters
def sort_routes_by_time():

    # import global variables
    global transport_routes

    # launch Shell sort with optimal intervals
    i = len(interval) - 1
    while i >= 0:
        i1 = i
        while i1 < len(transport_routes):
            i2 = i1 - interval[i]
            while i2 >= 0:
                # swap routes
                if transport_routes[i2].time > transport_routes[i2 + interval[i]].time:
                    curr_route = transport_routes[i2]
                    transport_routes[i2] = transport_routes[i2 + interval[i]]
                    transport_routes[i2 + interval[i]] = curr_route
                i2 = i2 - interval[i]
            i1 = i1 + 1
        i = i - 1

## This function sorts routes by money
#
# without any parameters
def sort_routes_by_money():

    # import global variables
    global transport_routes

    # launch Shell sort with optimal intervals
    i = len(interval) - 1
    while i >= 0:
        i1 = i
        while i1 < len(transport_routes):
            i2 = i1 - interval[i]
            while i2 >= 0:
                # swap routes
                if transport_routes[i2].cost > transport_routes[i2 + interval[i]].cost:
                    curr_route = transport_routes[i2]
                    transport_routes[i2] = transport_routes[i2 + interval[i]]
                    transport_routes[i2 + interval[i]] = curr_route
                i2 = i2 - interval[i]
            i1 = i1 + 1
        i = i - 1

## This function sorts routes using indicator of algorithm and type of query
#
# algol_ind is indicator of algorithm
#
# query is type of query
def sort_using_algol_ind(algol_ind, query):

    # default sort using time
    if len(algol_ind) == 0:
        sort_routes_by_time()
        return

    # sort using time or money
    if algol_ind == "Cost":
        sort_routes_by_money()
    elif algol_ind == "Time":
        sort_routes_by_time()
    elif algol_ind == "Default":
        # default mode defines type of sorting using number of queries
        if query.number_time_query > query.number_money_query:
            sort_routes_by_time()
        else:
            sort_routes_by_money()

## This function searches routes using ant algorithm
#
# transport_graph is array to describe connection between towns
#
# num_dep_town is id of departure town
#
# algol_ind is indicator of algorithm
#
# query is type of query
#
# return array of transport routes
def search_routes(transport_graph, num_dep_town, num_dest_town, algol_ind, query):

    # import global variables
    global number_ants
    global number_moves
    global time_change
    global transport_routes
    global ant_pheromones
    global add_constant

    # "launch" all ants
    i = 0
    while i < number_ants:

        # save current and end towns
        curr = num_dep_town
        end = num_dest_town
        # set of towns for routes
        set_towns = set([])
        i1 = 0
        i2 = -1

        while (i1 < number_moves) & (curr != end):
            # choose new current town
            currNew = randint(0, len(transport_graph.time_travel) - 1)
            if (not currNew in set_towns) & (transport_graph.time_travel[curr][currNew] != -1):
                # calculate full energy
                energy_full = calculate_energy(transport_graph, ant_pheromones, curr)
                # calculate energy of new town
                energy_new = (transport_graph.time_travel[curr][currNew] ** 0.4) * (ant_pheromones[curr][currNew] ** 0.6)
                # calculate probability
                prob = energy_new / energy_full
                prob_comp = random()
                if prob_comp < prob:
                    # add time to total counter with consideration of change
                    if i2 == -1:
                        transport_routes[i].time += transport_graph.time_travel[curr][currNew]
                    else:
                        transport_routes[i].time += time_change + transport_graph.time_travel[curr][currNew]
                    # add cost of new direction to total cost
                    transport_routes[i].cost += transport_graph.cost[curr][currNew]
                    # record new town
                    i2 = i2 + 1
                    curr = currNew
                    set_towns.add(curr)
                    transport_routes[i].towns[i2] = curr
                    transport_routes[i].towns.append(-1)
            i1 = i1 + 1

        # set big time for route that is impossible
        if curr != end:
            transport_routes[i].time = big_time

        # update ant pheromones
        dry_ant_pheromones()
        if (i2 >= 0) & (transport_routes[i].time != big_time):
            ant_pheromones[num_dest_town][transport_routes[i].towns[0]] = \
                ant_pheromones[num_dest_town][transport_routes[i].towns[0]] + \
                add_constant / (transport_routes[i].time * transport_routes[i].time * transport_routes[i].time)
            i1 = 0
            while i1 < i2:
                ant_pheromones[transport_routes[i].towns[i1]][transport_routes[i].towns[i1 + 1]] = \
                    ant_pheromones[transport_routes[i].towns[i1]][transport_routes[i].towns[i1 + 1]] + \
                    add_constant / (transport_routes[i].time * transport_routes[i].time * transport_routes[i].time)
                i1 = i1 + 1
        i = i + 1

    # sort routes using algorithm indicators
    sort_using_algol_ind(algol_ind, query)
    # return routes
    return transport_routes