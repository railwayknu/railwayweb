## @package file
# This module has all functions to read configuration and graph files
import os

## constant name of configuration file
config_file = "config.txt"
## constant to determine minuter in hour
minutesPerHour = 60

## structure to save user variables from configuration file
class UserVariables:

    ## url-address of web app
    url_address = ""
    ## name of Telegram bot
    name_bot = ""
    ## name of file to read time of travelling
    file_time_travel = ""
    ## name of file to read time of depatrure
    file_time_dep = ""
    ## name of file to read cost
    file_cost = ""
    ## name of file to read ant pheromones
    file_pheromones = ""
    ## name of file to read id of trains
    file_id = ""
    ## name of file to read type of trains
    file_type = ""
    ## number of routes for anonymous user
    num_routes_anom = 5
    ## number of routes for authenticated user
    num_routes_user = 7

## structure of graph for transport system
class TransportGraph:

    ## size of graph
    size = 0
    ## table to save time of travelling
    time_travel = []
    ## table to save time of departure
    time_dep = []
    ## table to save cost
    cost = []
    ## table to save ant pheromones
    pheromones = []
    ## table to save id of trains
    id = []
    ## table to save type of trains
    type = []

## This function reads configuration file
#
# without any parameters
def read_config_file():

    # read configuration file
    global config_file
    curr_folder = os.getcwd()
    file_reader = open(curr_folder + "/" + config_file, "r")

    # create structure to save user variables
    user_variables = UserVariables()

    # read lines of file
    line = file_reader.readline()
    while line:

        if line[0] != '#':

            # split row using symbol "="
            substrings = line.split('=')
            if len(substrings) == 2:

                # trim rows
                substrings[0] = str.strip(substrings[0])
                substrings[1] = str.strip(substrings[1])

                # handle rows and save result
                if substrings[0] == "URLAddress":
                    user_variables.url_address = substrings[1]
                elif substrings[0] == "NameBot":
                    user_variables.name_bot = substrings[1]
                elif substrings[0] == "FileTimeTravel":
                    user_variables.file_time_travel = substrings[1]
                elif substrings[0] == "FileTimeDep":
                    user_variables.file_time_dep = substrings[1]
                elif substrings[0] == "FileCost":
                    user_variables.file_cost = substrings[1]
                elif substrings[0] == "FilePheromones":
                    user_variables.file_pheromones = substrings[1]
                elif substrings[0] == "FileId":
                    user_variables.file_id = substrings[1]
                elif substrings[0] == "FileType":
                    user_variables.file_type = substrings[1]
                elif substrings[0] == "NumberRoutesAnom":
                    user_variables.num_routes_anom = int(substrings[1])
                elif substrings[0] == "NumberRoutesUser":
                    user_variables.num_routes_user = int(substrings[1])

        line = file_reader.readline()

    # close configuration file
    file_reader.close()

    # return user variables
    return user_variables

## This function reads file as integer table
#
# array is variable where table will be saved
#
# filename is name of file for reading
#
# size is number to describe size of railway graph
def read_file_as_int_table(array, filename, size):

    # open file
    curr_folder = os.getcwd()
    array_file = open(curr_folder + "/" + filename, "r")

    # go through all file
    i = 0
    while i < size:
        # read lines of file
        line = array_file.readline()
        line = str.strip(line)
        # divide row to substrings
        substrings = line.split(" ")
        # go through all substrings
        i1 = 0
        temp = []
        while i1 < size:
            # convert substring to integer numbers
            temp.append(int(substrings[i1]))
            i1 = i1 + 1
        # save result into array
        array.append(temp)
        i = i + 1

    # close file
    array_file.close()

## This function reads file as float table
#
# array is variable where table will be saved
#
# filename is name of file for reading
#
# size is number to describe size of railway graph
def read_file_as_float_table(array, filename, size):

    # open file
    curr_folder = os.getcwd()
    array_file = open(curr_folder + "/" + filename, "r")

    # go through all file
    i = 0
    while i < size:
        # read lines of file
        line = array_file.readline()
        line = str.strip(line)
        # divide row to substrings
        substrings = line.split(" ")
        # go through all substrings
        i1 = 0
        temp = []
        while i1 < size:
            # convert substring to float numbers
            temp.append(float(substrings[i1]))
            i1 = i1 + 1
        # save result into array
        array.append(temp)
        i = i + 1

    # close file
    array_file.close()

## This function reads file as time table
#
# array is variable where table will be saved
#
# filename is name of file for reading
#
# size is number to describe size of railway graph
def read_file_as_time_table(array, filename, size):

    # open file
    curr_folder = os.getcwd()
    array_file = open(curr_folder + "/" + filename, "r")

    # go through all file
    i = 0
    while i < size:
        # read lines of file22
        line = array_file.readline()
        line = str.strip(line)
        # divide row to substrings
        substrings = line.split(" ")
        # go through all substrings
        i1 = 0
        temp = []
        while i1 < size:
            # divide time to hour and minute
            substrings_time = substrings[i1].split(".")
            if len(substrings_time) == 1:
                # set value that "does not exist" in this array
                temp.append(-1)
            else:
                # convert substring to hour and minutes
                hour = int(substrings_time[0])
                minutes = int(substrings_time[1])
                temp.append(hour * minutesPerHour + minutes)
            i1 = i1 + 1
        # save result into array
        array.append(temp)
        i = i + 1

    # close file
    array_file.close()

## This function reads graph files
#
# user_variables is structure to save user parameters
#
# size is number to describe size of railway graph
def read_graph_files(user_variables, size):

    # create structure to save graphs
    transport_graph = TransportGraph()
    transport_graph.size = size

    # read graphs
    read_file_as_int_table(transport_graph.time_travel, user_variables.file_time_travel, size)
    read_file_as_time_table(transport_graph.time_dep, user_variables.file_time_dep, size)
    read_file_as_float_table(transport_graph.cost, user_variables.file_cost, size)
    read_file_as_float_table(transport_graph.pheromones, user_variables.file_pheromones, size)
    read_file_as_int_table(transport_graph.type, user_variables.file_type, size)
    read_file_as_int_table(transport_graph.id, user_variables.file_id, size)

    # to debug code
    """i = 0
    while i < size:
        i1 = 0
        while i1 < size:
            print("%5d " % transport_graph.time_dep[i][i1])
            i1 = i1 + 1
        print("\n")
        i = i + 1
    """

    # return transport graph
    return transport_graph
