## @package check
# This module has functions to provide verification or checking of different actions

## This function checks referrer ("main pages")
#
# request is received request from user
#
# url_address is url-address of web service
#
# return indicator of referrer
def check_referrer_main(request, url_address):

    # receive referrer
    name_referrer = request.META.get('HTTP_REFERER')

    # incorrect referrer
    if name_referrer == None:
        return False
    if len(name_referrer) < 2:
        return False

    # handle referrer
    i = len(name_referrer) - 2
    while i >= 0:
        if name_referrer[i] == '/':
            break
        i = i - 1
    name_referrer_mod = name_referrer[0:i]

    # check referrer using ulr-address
    ind = (name_referrer == url_address + "/") or (name_referrer_mod == url_address + "/auth")
    return ind

## This function checks referrer ("main page for authenticated user")
#
# request is received request from user
#
# url_address is url-address of web service
#
# return indicator of referrer
def check_referrer_main_auth(request, url_address):

    # receive referrer
    name_referrer = request.META.get('HTTP_REFERER')

    # incorrect referrer
    if name_referrer == None:
        return False
    if len(name_referrer) < 2:
        return False

    # handle referrer
    i = len(name_referrer) - 2
    while i >= 0:
        if name_referrer[i] == '/':
            break
        i = i - 1
    name_referrer_mod = name_referrer[0:i]

    # check referrer using ulr-address
    ind = name_referrer_mod == url_address + "/auth"
    return ind

## This function checks referrer ("login" and "map" pages)
#
# request is received request from user
#
# url_address is url-address of web service
#
# return indicator of referrer
def check_referrer_login_and_map(request, url_address):

    # receive referrer
    name_referrer = request.META.get('HTTP_REFERER')

    # incorrect referrer
    if name_referrer == None:
        return False
    if len(name_referrer) < 2:
        return False

    # handle referrer
    i = len(name_referrer) - 2
    while i >= 0:
        if name_referrer[i] == '/':
            break
        i = i - 1
    name_referrer_mod = name_referrer[0:i]

    # check referrer using ulr-address
    ind = (name_referrer == url_address + "/login/") or (name_referrer_mod == url_address + "/map")
    return ind

## This function checks referrer ("personal" page)
#
# request is received request from user
#
# url_address is url-address of web service
#
# return indicator of referrer
def check_referrer_personal(request, url_address):

    # receive referrer
    name_referrer = request.META.get('HTTP_REFERER')

    # incorrect referrer
    if name_referrer == None:
        return False
    if len(name_referrer) < 2:
        return False

    # handle referrer
    i = len(name_referrer) - 2
    while i >= 0:
        if name_referrer[i] == '/':
            break
        i = i - 1
    name_referrer_mod = name_referrer[0:i]

    # check referrer using ulr-address
    ind = (name_referrer_mod == url_address + "/personal")
    return ind

## This function checks using of mobile browse
#
# request is received request from user
#
# return indicator of mobile browser using
def check_mobile_browser(request):

    # find indicator whether this request is from mobile browser
    mobile_browser = request.user_agent.is_mobile
    # return answer
    return mobile_browser