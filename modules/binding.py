## @package binding
# This module has all functions to interact with dynamic C-library

# import library to call C-functions
import glob
import ctypes
import os

## name of dynamic library to calculate ant algorithm
name_library = "libalgol.so"
## name of filder to save dynamic library
dir_library = "libalgol"
## library file of C-functions
algol_libfile = None
## library of C-functions
algol_lib = None

## class for wrapper C-structure to describe transport route
class TransportRouteC(ctypes.Structure):
    ## fields of C-structure
    _fields_ = [("towns", ctypes.POINTER(ctypes.c_int)), ("time", ctypes.c_int), ("cost", ctypes.c_double)]

## class to describe transport route
class TransportRoute:

    ##__init__() class constructor
    def __init__(self):
        self.towns = []
        self.time = 0
        self.cost = 0.0

## This function sets parameters of C-functions
#
# without any parameters
def set_parameters():

    # set parameter of functions

    # function find_towns
    algol_lib.find_towns.restype = None
    algol_lib.find_towns.argtypes = [ctypes.POINTER(ctypes.c_char_p), ctypes.c_int,
                                     ctypes.c_char_p, ctypes.c_char_p,
                                     ctypes.POINTER(ctypes.c_bool),
                                     ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int)]

    # function create_transport_routes
    algol_lib.create_transport_routes.restype = None
    algol_lib.create_transport_routes.argtype = None

    # function clear_transport_routes
    algol_lib.clear_transport_routes.restype = None
    algol_lib.clear_transport_routes.argtype = None

    # function create_ant_pheromones
    algol_lib.create_ant_pheromones.restype = None
    algol_lib.create_ant_pheromones.argtype = [ctypes.POINTER(ctypes.c_int), ctypes.c_int]

    # function clear_ant_pheromones
    algol_lib.clear_ant_pheromones.restype = None
    algol_lib.clear_ant_pheromones.argtype = [ctypes.POINTER(ctypes.c_int), ctypes.c_int]

    # function search_routes
    algol_lib.search_routes.restype = ctypes.POINTER(TransportRouteC)
    algol_lib.search_routes.argtype = [ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_double),
                                       ctypes.c_int, ctypes.c_int, ctypes.c_int,
                                       ctypes.c_char_p, ctypes.c_int, ctypes.c_int,
                                       ctypes.POINTER(ctypes.c_int)]

## This function creates C-array (time) using transport graph
#
# transport_graph is graph of railway to search routes (time)
def create_c_transport_array_time(transport_graph):

    # calculate length and size of array for C-library
    tot_length = len(transport_graph) * len(transport_graph)
    size = len(transport_graph)

    # create array for C-library
    array_lib = (ctypes.c_int * tot_length)()

    # fill array of C-library
    i = 0
    while i < len(transport_graph):
        i1 = 0
        while i1 < len(transport_graph):
            array_lib[i * size + i1] = transport_graph[i][i1]
            i1 = i1 + 1
        i = i + 1

    # return answer
    return array_lib, size

## This function creates C-array (cost) using transport graph
#
# transport_graph is graph of railway to search routes (cost)
def create_c_transport_array_cost(transport_graph):

    # calculate length and size of array for C-library
    tot_length = len(transport_graph) * len(transport_graph)
    size = len(transport_graph)

    # create array for C-library
    array_lib = (ctypes.c_double * tot_length)()

    # fill array of C-library
    i = 0
    while i < len(transport_graph):
        i1 = 0
        while i1 < len(transport_graph):
            array_lib[i * size + i1] = transport_graph[i][i1]
            i1 = i1 + 1
        i = i + 1

    # return answer
    return array_lib

## This function initializes variable to work with C-library
#
# without any parameters
def init_library():

    # import global variables
    global algol_libfile
    global algol_lib

    # find the shared library, the path depends on the platform and Python version
    cwd = os.getcwd()
    algol_libfile = glob.glob(str(cwd + "/" + dir_library + "/" + name_library))[0]

    # open the shared library
    algol_lib = ctypes.CDLL(algol_libfile)
    # set parameters for C-function
    set_parameters()

## This function is wrapper of find_towns function in C-library
#
# towns is array of towns from database
#
# dep_town is name of departure town
#
# dest_town is name of destination town
#
# return indicator of success and id of towns in array
def find_towns(towns, dep_town, dest_town):

    # fill bytes in array for C-function
    towns_bytes = []
    for i in range(len(towns)):
        temp_row = towns[i]["name"].lower()
        towns_bytes.append(bytes(temp_row, 'utf-8'))

    # create array of towns for C-function
    towns_array = (ctypes.c_char_p * (len(towns_bytes) + 1))()
    towns_array[:-1] = towns_bytes

    # make towns from user in lover case
    dep_town = dep_town.lower()
    dest_town = dest_town.lower()

    # handle towns from user for C-library
    dep_town = dep_town.encode('utf-8')
    dest_town = dest_town.encode('utf-8')

    # initialize parameters for returning
    ind = ctypes.c_bool(False)
    num_dep_town = ctypes.c_int(-1)
    num_dest_town = ctypes.c_int(-1)

    # call function from C-library
    algol_lib.find_towns(towns_array, len(towns), dep_town, dest_town,
                         ctypes.byref(ind), ctypes.byref(num_dep_town), ctypes.byref(num_dest_town))

    # return answer
    return ind.value, num_dep_town.value, num_dest_town.value

## This function is wrapper of create_transport_routes function in C-library
#
# without any parameters
def create_transport_routes():
    # call function from C-library
    algol_lib.create_transport_routes()

## This function is wrapper of clear_transport_routes function in C-library
#
# without any parameters
def clear_transport_routes():
    # call function from C-library
    algol_lib.clear_transport_routes()

## This function is wrapper of create_transport_routes function in C-library
#
# transport_graph is graph of railway to search routes
def create_ant_pheromones(transport_graph):
    # create transport array for C-library
    array_lib, size = create_c_transport_array_time(transport_graph.time_travel)
    # call function from C-library
    algol_lib.create_ant_pheromones(array_lib, ctypes.c_int(size))

## This function is wrapper of clear_transport_routes function in C-library
#
# transport_graph is graph of railway to search routes
def clear_ant_pheromones(transport_graph):
    # create transport array for C-library
    array_lib, size = create_c_transport_array_time(transport_graph.time_travel)
    # call function from C-library
    algol_lib.clear_ant_pheromones(array_lib, ctypes.c_int(size))

## This function wrapper of search_routes function in C-library
#
# transport_graph is graph of railway to search routes
#
# num_dep_town is id of departure town
#
# algol_ind is indicator of algorithm
#
# query is type of query
#
# return array of transport routes
def search_routes(transport_graph, num_dep_town, num_dest_town, algol_ind, query):

    # create transport array for C-library
    array_lib_time, size = create_c_transport_array_time(transport_graph.time_travel)
    # create transport cost array for C-library
    array_lib_cost = create_c_transport_array_cost(transport_graph.cost)
    # handle row
    algol_ind = algol_ind.encode('utf-8')
    # number of routes
    num_routes = ctypes.c_int(0)
    # number of towns
    num_towns = ctypes.c_int(0)

    # call function from C-library
    transport_routes_c = algol_lib.search_routes(array_lib_time, array_lib_cost,
                                   ctypes.c_int(size), ctypes.c_int(num_dep_town), ctypes.c_int(num_dest_town),
                                   algol_ind, ctypes.c_int(query.number_time_query),
                                   ctypes.c_int(query.number_money_query),
                                   ctypes.byref(num_routes),
                                   ctypes.byref(num_towns))

    # convert c-type structure to Python structure
    i = 0
    transport_routes = []
    while i < num_routes.value:
        transport_routes.append(TransportRoute())
        # convert time and cost
        transport_routes[i].cost = transport_routes_c[i].cost
        transport_routes[i].time = transport_routes_c[i].time
        i1 = 0
        while i1 < num_towns.value:
            # convert array of towns
            transport_routes[i].towns.append(0)
            transport_routes[i].towns[i1] = transport_routes_c[i].towns[i1]
            if transport_routes[i].towns[i1] == -1:
                break
            i1 = i1 + 1
        i = i + 1

    # return answer
    return transport_routes
