## @package crypt
# This module has functions to provide security

## constant A for hash algorithm
A = 171
## constant B for hash algorithm
B = 11213
## constant M for hash algorithm
M = 53125


## This function calculates hash of row
#
# row is initial string
#
# return integer number that is hash
def calculate_hash(row):

    # calculate hash
    i = 0
    hash = 0
    while i < len(row):
        hash += (A*int(row[i]) + B) % M
        i = i + 1
        hash %= M

    # return hash
    return hash
