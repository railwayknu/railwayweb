#!/bin/bash

host="db"
port="3306"

echo "Checking DB for available"

until nc -zv "$host" "$port"; do
  echo "DB is unavailable - sleeping"
  sleep 1
done

echo "DB is up - executing command"

python manage.py inspectdb > railwayroute/models.py
python manage.py makemigrations && python manage.py migrate --fake-initial
python manage.py runserver 0.0.0.0:8000
