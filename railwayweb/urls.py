"""railwayweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from railwayroute import views

urlpatterns = [
    path('', views.main_page, name="main_page"),
    re_path('^map/(?:(?P<row_towns>[0-9&]+)/)?$', views.map_page, name="map_page"),
    path('login/', views.login_page, name="login_page"),
    re_path('^auth/(?:user_id=(?P<user_id>[0-9]+)/)$', views.auth_page, name="auth_page"),
    re_path('^personal/(?:user_id=(?P<user_id>[0-9]+)/)$', views.personal_page, name="personal page"),
    path('social-auth/', include('social_django.urls', namespace="social")),
    path('admin/', admin.site.urls),
]