FROM python:3.7

ENV PYTHONUNBUFFERED 1

RUN mkdir /app
WORKDIR /app

ADD . /app
RUN pip install -r requirements.txt
RUN apt-get update && apt-get install -y netcat wget
RUN cd /usr/local/lib/python3.7/site-packages/social/backends/ && wget -q https://raw.githubusercontent.com/python-social-auth/social-core/master/social_core/backends/linkedin.py -O linkedin.py
